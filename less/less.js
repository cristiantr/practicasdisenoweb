// LESS

@color: #4D926F;

#header {
  color: @color;
}
h2 {
  color: @color;
       span{
          color:#FFF; 
       }

}
/* CSS compilado */

#header {
 color: #4D926F;
   
}
h2 {
 color: #4D926F;
}
h2 span {
     color:#FFF;
}
/* Ejemplo de funcion  LESS */

.bloqueTexto(@color:#000,@background:#FFF){
       color:@color;
       background:@background;
}

#miBloqueDeTexto {
            .bloqueTexto(@color:#333,@background:#000);
}
#otroBloqueTexto{
          &nbsp;.bloqueTexto(@background:#000,@color:#FF00000);
}

/*RESULTADO REUTILIZANDO LA FUNCION  CSS */
#miBloqueDeTexto{
           color:#333;
           background:#000;
     }
#otroBloqueTexto{
    background:#000;
    color:#FF00000;
}