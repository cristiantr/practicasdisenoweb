'use strict';

function saveCarrera() {
	const nombre = document.getElementById('nombre').value;
	const codigo = document.getElementById('codigo').value;

	const carrera = {
		nombre,
		codigo
	};

	const carreras = insertToTable('carreras', carrera);
	renderTable('carreras', carreras);
}



function renderTable(tableName, tableData) {
	let table = jQuery(`#${tableName}_table`);	
	let rows = "";
	tableData.forEach((carrera, index) => {
		let row = `<tr><td>${carrera.nombre}</td><td>${carrera.codigo}</td>`;
		row += `<td> <a onclick="editEntity(this)" data-id="${carrera.id}" data-entity="${tableName}" class="link edit">Edit</a>  |  <a  onclick="deleteEntity(this);" data-id="${carrera.id}" data-entity="${tableName}" class="link delete">Delete</a>  </td>`;
		
		rows += row + '</tr>';
	});
	table.html(rows);
}

function editEntity(element) {
	
	const dataObj = jQuery(element).data();
    editElement(dataObj.entity, dataObj.id);
    let carrera = searchFromTable(dataObj.entity, dataObj.id)
	
}

function chargeEdit(carrera){
	
}

function deleteEntity(element) {
	const dataObj = jQuery(element).data();
	const newEntities = deleteFromTable(dataObj.entity, dataObj.id);
	renderTable(dataObj.entity, newEntities);
}

function loadTableData(tableName) {
	renderTable(tableName, getTableData(tableName));
}



function bindEvents() {
	jQuery('#add-carrera-button').bind('click', (element) => {
		saveCarrera();
	});
}

bindEvents();