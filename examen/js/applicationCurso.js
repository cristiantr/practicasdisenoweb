'use strict';

function saveCurso() {
	const carrera = document.getElementById('carrera').value;
	const nombre = document.getElementById('nombre').value;
	const credito = document.getElementById('credito').value;

	const curso = {
		carrera,
		nombre,
		credito
	};

	const cursos = insertToTable('cursos', curso);

	renderTable('cursos', cursos);
}



function renderTable(tableName, tableData) {
	let table = jQuery(`#${tableName}_table`);

	let rows = "";
	tableData.forEach((curso, index) => {
		let row = `<tr><td>${curso.carrera}</td><td>${curso.nombre}</td><td>${curso.credito}</td>`;
		row += `<td> <a onclick="editEntity(this)" data-id="${curso.id}" data-entity="${tableName}" class="link edit">Edit</a>  |  <a  onclick="deleteEntity(this);" data-id="${curso.id}" data-entity="${tableName}" class="link delete">Delete</a>  </td>`;
		
		rows += row + '</tr>';
	});
	table.html(rows);
}

function editEntity(element) {
	
	const dataObj = jQuery(element).data();
    editElement(dataObj.entity, dataObj.id);
    let curso = searchFromTable(dataObj.entity, dataObj.id);
    chargeEdit(curso)
	
}

function chargeEdit(curso){
	document.getElementById('carrera').value = curso.carrera;
    document.getElementById('nombre').value = curso.nombre;
	document.getElementById('credito').value = curso.credito;	
}
/*
Funcion eliminar
*/
function deleteEntity(element) {
	const dataObj = jQuery(element).data();
	const newEntities = deleteFromTable(dataObj.entity, dataObj.id);
	renderTable(dataObj.entity, newEntities);
}
/*
Funcion cargar la tabla
*/
function loadTableData(tableName) {
	renderTable(tableName, getTableData(tableName));
}



function bindEvents() {
	jQuery('#add-curso-button').bind('click', (element) => {
		saveCurso();
	});
}

bindEvents();